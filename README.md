# k8s-jenkins-toolbox

A docker image (alpine based) with several CI/CD tools to bo used in Jenkins pipelines.

## Tools included
- unzip
- helm
- awscli
- oci-cli
- kubectl
- git-crypt

## Supported tags and release links

The helm version is used to tag images:

- `3.2.4` [Dockerfile](https://gitlab.com/ganex-cloud/docker-images/k8s-jenkins-toolbox/tree/v3.2.4/Dockerfile)
- `2.14.3` [Dockerfile](https://gitlab.com/ganex-cloud/docker-images/k8s-jenkins-toolbox/tree/v2.14.3/Dockerfile)
- `2.14.1` [Dockerfile](https://gitlab.com/ganex-cloud/docker-images/k8s-jenkins-toolbox/tree/v2.14.1/Dockerfile)
- `2.14.0` [Dockerfile](https://gitlab.com/ganex-cloud/docker-images/k8s-jenkins-toolbox/tree/v2.14.0/Dockerfile)
- `2.13.1` [Dockerfile](https://gitlab.com/ganex-cloud/docker-images/k8s-jenkins-toolbox/tree/v2.13.1/Dockerfile)
- `2.12.3` [Dockerfile](https://gitlab.com/ganex-cloud/docker-images/k8s-jenkins-toolbox/tree/v2.12.3/Dockerfile)
- `2.11.0` [Dockerfile](https://gitlab.com/ganex-cloud/docker-images/k8s-jenkins-toolbox/tree/v2.11.0/Dockerfile)